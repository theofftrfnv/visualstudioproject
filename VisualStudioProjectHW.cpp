#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Animal voice\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!\n";
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woov!\n";
	}
};

class Rabbit : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Rabbit voice\n";
	}
};

int main()
{
	Rabbit r;
	Cat c;
	Dog d;
	Animal* animalVoices[] = {new Cat, new Dog, new Rabbit};

	for (int i = 0; i < 3; i++)
	{
		animalVoices[i]->Voice();
	}
}